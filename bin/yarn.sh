#!/usr/bin/env bash

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
source "${DIR}/../.env"

COMMAND=$@
export YARN_UBUNTU_VERSION=${YARN_UBUNTU_VERSION}

docker-compose \
    --file "${DIR}/../docker-compose.yml" \
    --project-name ${PROJECT_NAME} \
    run \
        --rm \
        --user $(id -u):$(id -g) \
        --volume /etc/passwd:/etc/passwd:ro \
        --volume /etc/group:/etc/group:ro \
        --volume ${PWD}:/${PWD} \
        --workdir="${PWD}" \
        yarn \
            yarn \
            ${COMMAND}
